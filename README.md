
# Hypervision Platform
> comandos no PowerShell

## Gerar imagem
```sh
docker build -t fhypervision-platform .
```

## Salvar a imagem
```sh
docker save -o hypervision-platform.tar hypervision-platform
```

## Executar a imagem
```sh
docker run -d --name=hypervision-platform -p 5001:80 -e URLS="http://none.com.br,http://none.com.br,http://none.com.br" -e KAFKA_GROUP_ID="HypervisionPlataform" -e KAFKA_SERVER="192.168.2.93:9092" -e SCHEMA_REGISTRY_URL="192.168.2.93:8081"  -e METEOROLOGICAL_CARD_TOPIC_NAME="Meteorological-Alerts" -e FIRE_CARD_TOPIC_NAME="Fire-Cards" -e CONNECTION_STRING="mongodb://root:hypervision@192.168.2.94:27017" --add-host=kafka-srv:192.168.2.93 hypervision-platform
```

# Banco de dados
Todos scripts, incluindo carga inicial para criação do banco, está na pasta scripts, na raiz do projeto

