﻿using Commons_Core.DAO;
using Commons_Core.Services.LogService;
using hypervision_platform.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace hypervision_platform.DAO
{
    public class RdoDAO : MongoBaseDAO<Card>
    {

        public RdoDAO(MongoContext context) : base(context)
        {
        }

        public List<Card> GetRDOById(string id)
        {
            var rdos = this.FilterBy(_ => _.CardId == id).ToList();

            if (rdos == null || rdos.Count == 0)
            {
                LogServiceConnection.Warning<string>($"RDO de ID {id} não encontrado");
                return default;
            }

            return rdos;
        }

        public List<Card> UpdateCard(Card card)
        {
            try
            {
                card.Id = new MongoDB.Bson.ObjectId();
                var cards = GetRDOById(card.CardId);

                if (cards == null || cards.Count == 0)
                {
                    LogServiceConnection.Warning<string>($"RDO de ID {card.CardId} não encontrado.");
                    return null;
                }

                cards.Add(card);

                this.InsertOne(card);

                LogServiceConnection.Debug<string>($"RDO de ID {card.CardId} atualizados.");

                return cards;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar um rdo: {ex.Message}");
                return null;
            }
        }

        public List<CardEvent> GetLastEventsByUser(string id)
        {
            var rdos = this.FilterBy(_ => _.CardId == id).Select(_ => _.Event).ToList();
            var grouped = rdos.GroupBy(_ => _.User).Select(g => new { g.Key, events = g.ToList() });
            var lastEvents = new List<CardEvent>();
            foreach (var item in grouped)
            {
                var last = item.events.OrderBy(_ => _.Date).Last();
                lastEvents.Add(last);
            }
            return lastEvents;
        }


        public List<Card> GetRDOByIdOrThrowsException(string id)
        {
            var rdos = GetRDOById(id);

            if (rdos == null)
            {
                LogServiceConnection.Error<string>($"RDO {id} não encontrado");
                throw new FileNotFoundException();
            }

            return rdos;
        }

        /// <summary>
        /// Remove o RDO: é usado apenas para excluir RDO mesclado.
        /// É diferente da funcionalidade de remoção de cartão que é apenas uma ação no cartão 
        /// feita por usuário e visualizada apenas para tal usuário que realizou a remoção.
        /// </summary>
        public void DeleteRDOById(string id)
        {
            var rdo = this.FilterBy(_ => _.CardId == id).ToList();

            if (rdo != null && rdo.Count > 0)
            {
                this.DeleteMany(_ => _.CardId == id);
            }
            else
            {
                LogServiceConnection.Warning<string>($"RDO de ID {id} não encontrado");
            }
        }
    }
}
