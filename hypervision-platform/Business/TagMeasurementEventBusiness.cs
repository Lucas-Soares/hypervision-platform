using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using KafkaSchemas.Avro;
using System;
using System.Linq;

namespace hypervision_platform
{
    public class TagMeasurementEventBusiness : BaseEventBusiness<TagMeasurementAlerts>
    {
        private readonly RdoDAO _rdoDAO;

        public TagMeasurementEventBusiness(RdoDAO rdoDAO) : base(rdoDAO)
        {
            _rdoDAO = rdoDAO;
        }
        internal override Card SaveOrUpdateRDOFromTopic(TagMeasurementAlerts data)
        {
            try
            {
                var card = new Card
                {
                    CardId = data.IDEVENTO.ToString(),
                    Date = data.DATAHORAATUAL.ToUniversalTime(),
                    EventStart = data.DATAHORAATUAL.ToUniversalTime(),
                    Dispached = false,
                    Severity = Severity.ALERT,
                    EventType = EventType.TagMeasurement,
                    CardDetails = new CardDetails
                    {
                        ObjId = data.OBJID,
                        EventId = data.IDEVENTO,
                        Tag = data.TAG,
                        TagRem = data.TAGREM,
                        Class = data.CLASSE,
                        Description = data.DESCRICAO,
                        AlarmRange = data.FAIXAALARME,
                        AlarmStatus = data.ESTADOALARME,
                        AlarmValue = data.VALORALARME,
                        AreaSegEnt = data.AREASEGENT,
                        Blink = data.BLINK,
                        CurrentDatetime = data.DATAHORAATUAL,
                        Type = data.TIPO,
                        EntityType = data.TIPOENTIDADE,
                        DifferenceAlarm = data.Diferenca,
                        Group = data.GRUPO,
                        HistoricalXPlot = data.PlotXHistorico,
                        HistoricalYPlot = data.PlotYHistorico,
                        Normalize = data.NORMALIZA,
                        NormalizeStatus = data.ESTADONORMALIZA,
                        NormalizeValue = data.VALORNORMALIZA,
                        NormalizeRange = data.FAIXANORMALIZA,
                        Origin = data.ORIGEM,
                        IDPTOVINC = data.IDPTOVINC,
                        OriginDatetime = data.DATAHORAORIGEM,
                        OriginNormDatetime = data.DATAHORANORMORIGEM,
                        PredictedAlarm = data.ValorPrevisto,
                        PredictedXPlot = data.PlotXPrevisoes,
                        PredictedYPlot = data.PlotYPrevisoes,
                        Priority = data.PRIORIDADE,
                        RepositoryDatetime = data.DATAHORAREPOSITORIO,
                        RepositoryNormDatetime = data.DATAHORANORMREPOSITORIO,
                        Synoptic = data.SINOTICO
                    }
                };

                var rdoForEvent = _rdoDAO.FilterBy(_ => _.CardId == data.IDEVENTO.ToString()).ToList();

                if (rdoForEvent == null || rdoForEvent.Count == 0)
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento recebido",
                        Type = ActionType.None
                    };

                    LogServiceConnection.Debug<string>("Evento de alerta de medição recebido.");
                }
                else
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento atualizado",
                        Type = rdoForEvent.Last().Event.Type,
                        User = rdoForEvent.Last().Event.User
                    };

                    LogServiceConnection.Debug<string>("Evento de alerta de medi��o atualizado.");
                }

                _rdoDAO.InsertOne(card);
                LogServiceConnection.Debug<string>("Evento de alerta de medi��o persistido.");

                return card;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar um rdo: {ex.Message}");
                return null;
            }
        }

        internal override void SetConfigurations()
        {
            this.TopicName = Environment.GetEnvironmentVariable("TAG_MEASUREMENT_TOPIC_NAME");
            this.RDO_Name = "TagMeasurementCard";
        }
    }
}