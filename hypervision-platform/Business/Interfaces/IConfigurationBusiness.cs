﻿using hypervision_platform.DVO;
using System.Threading.Tasks;

namespace hypervision_platform.Business.Interfaces
{
    public interface IConfigurationBusiness
    {
        ConfigurationVO GetConfiguration();
        Task<ConfigurationVO> SaveConfiguration(ConfigurationVO configuration);
    }
}
