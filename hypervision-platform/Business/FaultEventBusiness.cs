using KafkaSchemas.Avro;
using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hypervision_platform
{
    public class FaultEventBusiness : BaseEventBusiness<FaultCards>
    {
        private readonly RdoDAO _rdoDAO;
        public FaultEventBusiness(RdoDAO rdoDAO) : base(rdoDAO)
        {
            _rdoDAO = rdoDAO;
        }
        internal override Card SaveOrUpdateRDOFromTopic(FaultCards data)
        {
            try
            {

                //TODO: revisar localtime
                var card = new Card
                {
                    CardId = data.Id,
                    Date = DateTime.Parse(data.Date).ToUniversalTime(),
                    Dispached = false,
                    Severity = Severity.ALARM,
                    Level = string.Join(", ", data.Substations),
                    EventStart = DateTime.Parse(data.Date).ToUniversalTime(),
                    EventType = EventType.Fault,
                    CardDetails = new CardDetails()
                    {
                        Tag = string.Join(",", data.OpenEquipments),
                        Properties = new List<List<Property>>()
                    }
                };

                data.Protections.OrderBy(p => DateTime.Parse(p.Date).ToUniversalTime()).ToList().ForEach(p =>
                {
                    var properties = new List<Property>();
                    properties.Add(new Property
                    {
                        Field = "date",
                        Label = "Data",
                        Value = DateTime.Parse(p.Date).ToUniversalTime().ToString("o"),
                        Name = "date",
                        Type = PropertyType._date
                    });
                    properties.Add(new Property
                    {
                        Field = "tag",
                        Label = "Tag",
                        Value = p.Tag,
                        Name = "tag",
                        Type = PropertyType._string
                    });
                    properties.Add(new Property
                    {
                        Field = "description",
                        Label = "Descrição",
                        Value = p.Description,
                        Name = "description",
                        Type = PropertyType._string
                    });
                    properties.Add(new Property
                    {
                        Field = "protocol",
                        Label = "Protocolo",
                        Value = p.Protocol,
                        Name = "protocol",
                        Type = PropertyType._string
                    });
                     properties.Add(new Property
                    {
                        Field = "actuation",
                        Label = "Atuação",
                        Value = p.Actuation.ToUpper() == "S" ? "Suspeito" : "Correto",
                        Name = "actuation",
                        Type = PropertyType._string
                    });
                    properties.Add(new Property
                    {
                        Field = "substation",
                        Label = "Subestação",
                        Value = p.Substation,
                        Name = "substation",
                        Type = PropertyType._string
                    });
                    properties.Add(new Property
                    {
                        Field = "synoptic",
                        Label = "Sinótico",
                        Value = p.Synoptic,
                        Name = "synoptic",
                        Type = PropertyType._string
                    });
                    card.CardDetails.Properties.Add(properties);
                });

                var rdoForEvent = _rdoDAO.FilterBy(_ => _.CardId == card.CardId).ToList();

                if (rdoForEvent == null || rdoForEvent.Count == 0)
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento recebido",
                        Type = ActionType.None
                    };
                    LogServiceConnection.Debug<string>("Evento de Falta recebido.");
                }
                else
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento atualizado",
                        Type = rdoForEvent.Last().Event.Type,
                        User = rdoForEvent.Last().Event.User
                    };
                    LogServiceConnection.Debug<string>("Evento de Falta atualizado.");
                }

                _rdoDAO.InsertOne(card);

                LogServiceConnection.Debug<string>("Evento de Falta persistido.");
                return card;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar um rdo de Falta: {ex.Message}");
                return null;
            }
        }

        internal override void SetConfigurations()
        {
            this.TopicName = Environment.GetEnvironmentVariable("FAULT_CARD_TOPIC_NAME");
            this.RDO_Name = "FaultCard";
        }
    }
}