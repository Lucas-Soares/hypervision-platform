using Commons_Core.Services.LogService;
using hypervision_platform.Business.Interfaces;
using hypervision_platform.DAO;
using hypervision_platform.DVO;
using hypervision_platform.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace hypervision_platform
{
    public class ConfigurationBusiness : IConfigurationBusiness
    {
        public readonly string kafkaServer = Environment.GetEnvironmentVariable("KAFKA_SERVER");
        private readonly ConfigurationDAO _configurationDAO;

        public ConfigurationBusiness()
        {
            _configurationDAO = new ConfigurationDAO(new MongoContext());
        }

        public ConfigurationVO GetConfiguration()
        {
            try
            {
                var configuration = _configurationDAO.GetConfiguration();

                return EntityToDTO(configuration);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro recuperar configuração: {ex.Message}");

                return default;
            }
        }

        public async Task<ConfigurationVO> SaveConfiguration(ConfigurationVO configurationDTO)
        {
            try
            {
                var configuration = _configurationDAO.FindAll().FirstOrDefault();

                if (configuration == null)
                {
                    configuration = new Configuration(configurationDTO.MinLogLevel, configurationDTO.KafkaServer, configurationDTO.CategoryCardConfigurations,
                                                        configurationDTO.TimelineConfigurations, configurationDTO.EventFeedConfigurations, configurationDTO.NavbarConfigurations);
                    await _configurationDAO.InsertOneAsync(configuration);
                }
                else
                {
                    configuration = new Configuration();
                    configuration.Update(configurationDTO.MinLogLevel, configurationDTO.KafkaServer, configurationDTO.CategoryCardConfigurations,
                                            configurationDTO.TimelineConfigurations, configurationDTO.EventFeedConfigurations, configurationDTO.NavbarConfigurations);
                    await _configurationDAO.UpdateOneAsync(configuration);
                }

                if (configuration.MinLogLevel != configurationDTO.MinLogLevel)
                {
                    UpdateNewMinLogLevel(configuration.MinLogLevel);
                }

                LogServiceConnection.Info<string>($"Nova configuração salva para o sistema.");

                return EntityToDTO(configuration);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao salvar configuração: {ex.Message}");

                return default;
            }
        }

        private static void UpdateNewMinLogLevel(LogLevel logLevel)
        {
            LogServiceConnection.SetMinLogLevel(logLevel.ToString());
            LogServiceConnection.Debug<string>($"Nível mínimo de log reconfigurado: {logLevel}");
        }

        private ConfigurationVO EntityToDTO(Configuration configuration)
        {
            if (configuration == null)
                return new ConfigurationVO();

            return new ConfigurationVO
            {
                Id = configuration.Id.ToString(),
                MinLogLevel = configuration.MinLogLevel,
                KafkaServer = configuration.KafkaServer,
                CategoryCardConfigurations = configuration.CategoryCardConfigurations,
                TimelineConfigurations = configuration.TimelineConfigurations,
                EventFeedConfigurations = configuration.EventFeedConfigurations,
                NavbarConfigurations = configuration.NavbarConfigurations
            };
        }

        /// <summary>
        /// Canverte a área de atuação detalhada para área simplificada
        /// </summary>
        /// <param name="detailedArea"></param>
        /// <returns></returns>
        public static string ConvertArea(string detailedArea)
        {
            if (detailedArea.Contains("AT-METRO") || detailedArea.Contains("MT-METRO"))
            {
                return "METROPOLITANO";
            }
            if (detailedArea.Contains("AT-MANTI") || detailedArea.Contains("MT-MANTI"))
            {
                return "MANTIQUEIRA";
            }
            if (detailedArea.Contains("AT-SUL") || detailedArea.Contains("MT-SUL"))
            {
                return "SUL";
            }
            if (detailedArea.Contains("AT-NORTE") || detailedArea.Contains("MT-NORTE"))
            {
                return "NORTE";
            }
            if (detailedArea.Contains("AT-LESTE") || detailedArea.Contains("MT-LESTE"))
            {
                return "LESTE";
            }
            if (detailedArea.Contains("AT-OESTE") || detailedArea.Contains("MT-OESTE"))
            {
                return "OESTE";
            }
            if (detailedArea.Contains("AT-TRIAN") || detailedArea.Contains("MT-TRIAN"))
            {
                return "TRIANGULO";
            }
            return null;
        }

    }
}