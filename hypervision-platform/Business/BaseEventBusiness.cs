﻿using Commons_Core.Helpers.Kafka;
using Commons_Core.Services.LogService;
using Confluent.Kafka;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using System;
using System.Threading.Tasks;

namespace hypervision_platform
{
    public abstract class BaseEventBusiness<TCardSchema> : GenericEventBusiness, IDisposable
    {
        private static readonly string _groupId = Environment.GetEnvironmentVariable("KAFKA_GROUP_ID");
        private static readonly string _kafkaServer = Environment.GetEnvironmentVariable("KAFKA_SERVER");
        private static readonly string _schemaServer = Environment.GetEnvironmentVariable("SCHEMA_REGISTRY_URL");

        private static Task Task { get; set; }

        internal delegate Task CardReceivedHandler(Card card);
        internal event CardReceivedHandler CardReceivedEvent;
        internal string TopicName { get; set; }
        internal string RDO_Name { get; set; }

        public BaseEventBusiness(RdoDAO rdoDAO): base(rdoDAO)
        {
            SetConfigurations();
        }

        public void StartTask()
        {
            try
            {
                if (Task == null)
                {
                    Task = new Task(new Action(ConsumeTopic));
                    Task.Start();
                    LogServiceConnection.Debug<string>("Thread do consumidor iniciada.");
                }
            }
            catch (System.Exception ex)
            {
                LogServiceConnection.Error<string>(ex.Message);
            }
        }

        private void ConsumeTopic()
        {
            if (string.IsNullOrEmpty(TopicName))
            {
                LogServiceConnection.Error<string>($"Topic name is empty or null: {TopicName}");
                return;
            }
            var consumer = new AvroConsumerHelper<string, TCardSchema>(_kafkaServer, _groupId, _schemaServer);
            consumer.Subscribe(TopicName);
            try
            {
                while (true)
                {
                    try
                    {
                        var consumeResult = consumer.Consume();
                        if (consumeResult != null)
                        {
                            LogServiceConnection.Debug<TCardSchema>($"Message received from topic: {TopicName} | {consumeResult.Message.Value}");
                            var card = consumeResult.Message.Value;
                            var result = SaveOrUpdateRDOFromTopic(card);
                            CardReceivedEvent(result);
                        }
                    }
                    catch (ConsumeException e)
                    {
                        LogServiceConnection.Error<string>($"Erro ao tentar consumir mensagens do tópico {TopicName}: {e}");
                    }
                }
            }
            catch (OperationCanceledException)
            {
                consumer.Dispose();
            }

        }

        internal abstract Card SaveOrUpdateRDOFromTopic(TCardSchema data);
        internal abstract void SetConfigurations();

        public void Dispose()
        {
            Task.Dispose();
        }
    }
}