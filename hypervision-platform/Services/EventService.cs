﻿using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.DVO;
using hypervision_platform.Models;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hypervision_platform.Services
{
    public class EventService
    {
        private readonly IHubContext<EventHub> _hub;
        private readonly RdoDAO _rdoDAO;
        public EventService(IHubContext<EventHub> hub)
        {
            _rdoDAO = new RdoDAO(new MongoContext());
            _hub = hub;
        }

        /// <summary>
        /// Finaliza eventos expirados
        /// </summary>
        /// <param name="cards"></param>
        /// <returns></returns>
        public async Task FinishExpiredEventAsync(List<CardVO> cards)
        {
            foreach (var card in cards)
            {
                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                CardVO updatedCard = business.AddAction(ActionType.Expire, card.Id, "");

                await EventHub.SendMessageToAll(_hub, updatedCard);
                LogServiceConnection.Info<string>("O cartão expirou e foi finalizado pelo sistema.");
            }

        }

        /// <summary>
        /// Aceita os eventos recebidos por parâmetro
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task AcceptEventAsync(List<CardVO> cards, string user)
        {
            foreach (var card in cards)
            {
                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                CardVO updatedCard = business.AddAction(ActionType.Accept, card.Id, user);

                await EventHub.SendMessageToAll(_hub, updatedCard);
                LogServiceConnection.Info<string>($"O usuário {user} aceitou o cartão");
            }

        }

        /// <summary>
        /// Rejeita os eventos recebidos por parâmetro
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="user"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task RejectEventAsync(List<CardVO> cards, string user, string comment)
        {
            foreach (var card in cards)
            {
                List<string> fields = EventHub.GetFieldsByUser(user);

                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                CardVO updatedCard = business.AddAction(ActionType.Reject, card.Id, user, comment, fields);

                await EventHub.SendMessageToUser(_hub, user, updatedCard);
                LogServiceConnection.Info<string>($"O usuário {user} rejeitou o cartão. Comentário: {comment}");
            }

        }

        /// <summary>
        /// Deleta os eventos recebidos por parâmetro
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="user"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task DeleteEventAsync(List<CardVO> cards, string user)
        {
            foreach (var card in cards)
            {

                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                CardVO updatedCard = business.AddAction(ActionType.Delete, card.Id, user);

                await EventHub.SendMessageToUser(_hub, user, updatedCard);
                LogServiceConnection.Info<string>($"O usuário {user} removeu o cartão.");
            }

        }

        public async Task AddActionOnEvent(List<CardVO> cards, ActionType type, string user, string comment)
        {
            foreach (var card in cards)
            {
                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                CardVO updatedCard = business.AddAction(type, card.Id, user, comment);

                await EventHub.SendMessageToAll(_hub, updatedCard);
                LogServiceConnection.Info<string>($"O usuário {user} incluiu ação no cartão. Comentário: {comment}");
            }
        }

        /// <summary>
        /// Rejeita os eventos recebidos por parâmetro
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="user"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task ForwardEventAsync(List<CardVO> cards, string user, string receiverUser, string comment)
        {
            foreach (var card in cards)
            {
                var business = EventBusinessFactory.CreateBusinessByType(card.EventType, _rdoDAO);

                if (business.IsRDOForwarded(card.Id))
                {
                    LogServiceConnection.Error<string>($"Evento {card.Id} já foi encaminhado, não pode ser encaminhado novamente. Usuário: {user}");
                    continue;
                }

                CardVO updatedCard = business.AddAction(ActionType.Forward, card.Id, user, comment, null, receiverUser);

                await EventHub.SendMessageToAll(_hub, updatedCard);
                LogServiceConnection.Info<string>($"O usuário {user} encaminhou o cartão para {receiverUser}. Comentário: {comment}");
            }
        }

        /// <summary>
        /// Rejeita os eventos recebidos por parâmetro
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="user"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task MergeEventsAsync(List<CardVO> cards, string user, string comment)
        {
            if (cards.Count <= 1)
            {
                LogServiceConnection.Error<string>($"São necessários pelo menos dois cartões para serem mesclados.");
                return;
            }

            var mainCard = cards[0];
            cards.RemoveAt(0);

            // TODO: CDF-696: alertas de manobra não podem ser mesclados

            var business = EventBusinessFactory.CreateBusinessByType(mainCard.EventType, _rdoDAO);

            Card updatedCard = business.MergeRDOs(user, mainCard, cards, comment); ;

            await EventHub.SendMessageToAll(_hub, updatedCard);
            LogServiceConnection.Info<string>($"O usuário {user} mesclou os cartões.");
        }

    }
}
