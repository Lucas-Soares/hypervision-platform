﻿using Commons_Core.Services.LogService;
using hypervision_platform.Business.Interfaces;
using hypervision_platform.DVO;
using hypervision_platform.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace hypervision_platform.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    public class ConfigurationController : Controller
    {
        private readonly IConfigurationBusiness _configurationBusiness;
        private readonly IConfigurationService _configurationService;
        public ConfigurationController(IConfigurationBusiness configurationBusiness, IConfigurationService configurationService)
        {
            _configurationBusiness = configurationBusiness;
            _configurationService = configurationService;
        }

        [HttpGet("GetFields")]
        public IActionResult GetFields()
        {
            try
            {
                var fields = _configurationService.GetFields();
                LogServiceConnection.Debug<string>($"Recuperou as áreas de atuação");
                return Ok(fields);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar buscar áreas de atuação: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetUrls")]
        public IActionResult GetUrls()
        {
            try
            {
                var urls = _configurationService.GetUrls();
                LogServiceConnection.Debug<string>($"Recuperou as url dos apps");
                return Ok(urls);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar buscar url dos apps: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetConfiguration")]
        public IActionResult GetConfiguration()
        {
            try
            {
                var configuration = _configurationBusiness.GetConfiguration();
                LogServiceConnection.Debug<string>($"Recuperou configuração");
                return Ok(configuration);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar recuperar configuração: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPost("Save/{user}")]
        public async Task<IActionResult> Save([FromBody] ConfigurationVO configuration, string user)
        {
            try
            {
                var result = await _configurationBusiness.SaveConfiguration(configuration);
                LogServiceConnection.Info<string>($"O usuário {user} salvou a configuração");
                return Ok(result);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar a configuração: {ex.Message}");
                return Problem(ex.Message);
            }
        }
        [HttpPost("OnPostUploadAsync")]
        public IActionResult OnPostUploadAsync(IFormFile file)
        {
            try
            {
                var result = _configurationService.UploadSoundFile(file);
                return Ok(result);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar arquivo de som: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetActiveUsers")]
        public IActionResult GetActiveUsers()
        {
            try
            {
                var users = EventHub.GetRegisteredUsers();
                LogServiceConnection.Debug<string>($"Recuperou os usuários ativos");
                return Ok(users);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar buscar os usuários ativos: {ex.Message}");
                return Problem(ex.Message);
            }
        }

    }
}
