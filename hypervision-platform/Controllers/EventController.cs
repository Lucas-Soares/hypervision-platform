using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.DVO;
using hypervision_platform.Models;
using hypervision_platform.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hypervision_platform.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    public class EventController : Controller
    {
        private readonly IHubContext<EventHub> _hub;
        private readonly RdoDAO _rdoDAO;
        public EventController(IHubContext<EventHub> hub)
        {
            _rdoDAO = new RdoDAO(new MongoContext());
            _hub = hub;
        }

        [HttpGet("GetRDO/{type}/{id}")]
        public ActionResult GetRDO(EventType type, string id)
        {
            try
            {
                List<CardVO> rdo = null;
                switch (type)
                {
                    case EventType.Meteorological:
                        rdo = new MetereologicalEventBusiness(_rdoDAO).GetRDOById(id);
                        break;
                    case EventType.Fire:
                        rdo = new FireEventBusiness(_rdoDAO).GetRDOById(id);
                        break;
                    case EventType.TagMeasurement:
                        rdo = new TagMeasurementEventBusiness(_rdoDAO).GetRDOById(id);
                        break;
                    case EventType.RA:
                        rdo = new RAEventBusiness(_rdoDAO).GetRDOById(id);
                        break;
                    case EventType.Fault:
                        rdo = new FaultEventBusiness(_rdoDAO).GetRDOById(id);
                        break;
                }
                LogServiceConnection.Info<string>($"Recuperou o rdo {rdo.Last().Id}");
                return Ok(rdo);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao recuperar o rdo: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("AcceptEvent/{user}")]
        public async Task<ActionResult> AcceptEventAsync([FromBody] List<CardVO> cards, string user)
        {
            try
            {
                await new EventService(_hub).AcceptEventAsync(cards, user);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar aceitar um cartão: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("RejectEvent/{user}/{comment}")]
        public async Task<ActionResult> RejectEventAsync([FromBody] List<CardVO> cards, string user, string comment)
        {
            try
            {
                await new EventService(_hub).RejectEventAsync(cards, user, comment);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar rejeitar um cartão: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("DeleteEvent/{user}")]
        public async Task<ActionResult> DeleteEventAsync([FromBody] List<CardVO> cards, string user)
        {
            try
            {
                await new EventService(_hub).DeleteEventAsync(cards, user);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar remover um cartão: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("AddActionOnEvent/{type}/{user}/{comment}/")]
        public async Task<ActionResult> AddActionOnEvent([FromBody] List<CardVO> cards, ActionType type, string user, string comment)
        {
            try
            {
                await new EventService(_hub).AddActionOnEvent(cards, type, user, comment);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar atualizar um cartão: {ex.Message}");
                return Problem(ex.Message);
            }
        }


        [HttpPut("FinishExpiredEvent")]
        public async Task<ActionResult> FinishExpiredEventAsync([FromBody] List<CardVO> cards)
        {
            try
            {
                await new EventService(_hub).FinishExpiredEventAsync(cards);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar finalizar um cartão vencido: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("ForwardEvent/{user}/{receiverUser}/{comment}")]
        public async Task<ActionResult> ForwardEventAsync([FromBody] List<CardVO> cards, string user, string receiverUser, string comment)
        {
            try
            {
                await new EventService(_hub).ForwardEventAsync(cards, user, receiverUser, comment);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar encaminhar um cartão: {ex.Message}");
                return Problem(ex.Message);
            }
        }

        [HttpPut("MergeEvents/{user}/{comment}")]
        public async Task<ActionResult> MergeEventsAsync([FromBody] List<CardVO> cards, string user, string comment)
        {
            try
            {
                await new EventService(_hub).MergeEventsAsync(cards, user, comment);
                return Ok(cards);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao mesclar cartões: {ex.Message}");
                return Problem(ex.Message);
            }
        }

    }
}