﻿namespace KafkaSchemas.Avro
{
    using global::Avro;
    using global::Avro.Specific;
    using System;
    using System.Collections.Generic;

    public partial class TagMeasurementAlerts : ISpecificRecord
    {
        public static Schema _SCHEMA = Schema.Parse(@"{""name"": ""TagMeasurementAlerts"",""namespace"": ""KafkaSchemas.Avro"",""type"": ""record"",""fields"": [{""doc"": ""Data e hora da análise."",""name"": ""DataHora"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""name"": ""ValorAlarme"",""type"": ""double""},{""name"": ""ValorPrevisto"",""type"": ""double""},{""doc"": ""Diferença percentual entre o valor do alerta e o valor previsto."",""name"": ""Diferenca"",""type"": ""double""},{""name"": ""PlotXHistorico"",""type"": {""items"": ""long"",""type"": ""array""}},{""name"": ""PlotYHistorico"",""type"": {""items"": ""double"",""type"": ""array""}},{""name"": ""PlotXPrevisoes"",""type"": {""items"": ""long"",""type"": ""array""}},{""name"": ""PlotYPrevisoes"",""type"": {""items"": ""double"",""type"": ""array""}},{""name"": ""IDEVENTO"",""type"": ""long""},{""name"": ""DATAHORAORIGEM"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""default"": null,""name"": ""DATAHORANORMORIGEM"",""type"": [""null"",{""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}]},{""name"": ""DATAHORAATUAL"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""name"": ""DATAHORAREPOSITORIO"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""default"": null,""name"": ""DATAHORANORMREPOSITORIO"",""type"": [""null"",{""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}]},{""name"": ""OBJID"",""type"": ""long""},{""name"": ""TAG"",""type"": ""string""},{""name"": ""AREASEGENT"",""type"": ""string""},{""default"": null,""name"": ""SINOTICO"",""type"": [""null"",""string""]},{""name"": ""GRUPO"",""type"": ""string""},{""name"": ""CLASSE"",""type"": ""string""},{""name"": ""DESCRICAO"",""type"": ""string""},{""default"": null,""name"": ""TAGREM"",""type"": [""null"",""string""]},{""name"": ""PRIORIDADE"",""type"": ""int""},{""name"": ""ESTADOALARME"",""type"": ""string""},{""name"": ""VALORALARME"",""type"": ""double""},{""default"": null,""name"": ""ESTADONORMALIZA"",""type"": [""null"",""string""]},{""default"": null,""name"": ""VALORNORMALIZA"",""type"": [""null"",""double""]},{""default"": null,""name"": ""IDPTOVINC"",""type"": [""null"",""long""]},{""name"": ""ORIGEM"",""type"": ""int""},{""name"": ""NORMALIZA"",""type"": ""int""},{""name"": ""FAIXAALARME"",""type"": ""int""},{""default"": null,""name"": ""FAIXANORMALIZA"",""type"": [""null"",""int""]},{""name"": ""BLINK"",""type"": ""int""},{""name"": ""TIPOENTIDADE"",""type"": ""int""},{""name"": ""TIPO"",""type"": ""int""}]}");

        /// <summary>
        /// Data e hora da análise.
        /// </summary>
        private DateTime _DataHora;
        private double _Diferenca;
        private double _ValorAlarme;
        private double _ValorPrevisto;
        private List<long> _PlotXHistorico;
        private List<double> _PlotYHistorico;
        private List<long> _PlotXPrevisoes;
        private List<double> _PlotYPrevisoes;
        private long _IDEVENTO;
        private System.DateTime _DATAHORAORIGEM;
        private System.Nullable<System.DateTime> _DATAHORANORMORIGEM;
        private System.DateTime _DATAHORAATUAL;
        private System.DateTime _DATAHORAREPOSITORIO;
        private System.Nullable<System.DateTime> _DATAHORANORMREPOSITORIO;
        private long _OBJID;
        private string _TAG;
        private string _AREASEGENT;
        private string _SINOTICO;
        private string _GRUPO;
        private string _CLASSE;
        private string _DESCRICAO;
        private string _TAGREM;
        private int _PRIORIDADE;
        private string _ESTADOALARME;
        private double _VALORALARME;
        private string _ESTADONORMALIZA;
        private System.Nullable<System.Double> _VALORNORMALIZA;
        private System.Nullable<System.Int64> _IDPTOVINC;
        private int _ORIGEM;
        private int _NORMALIZA;
        private int _FAIXAALARME;
        private System.Nullable<System.Int32> _FAIXANORMALIZA;
        private int _BLINK;
        private int _TIPOENTIDADE;
        private int _TIPO;

        public virtual Schema Schema
        {
            get
            {
                return TagMeasurementAlerts._SCHEMA;
            }
        }
        public DateTime DataHora
        {
            get
            {
                return this._DataHora;
            }
            set
            {
                this._DataHora = value;
            }
        }
        public double Diferenca
        {
            get
            {
                return this._Diferenca;
            }
            set
            {
                this._Diferenca = value;
            }
        }
        public double ValorAlarme
        {
            get
            {
                return this._ValorAlarme;
            }
            set
            {
                this._ValorAlarme = value;
            }
        }
        public double ValorPrevisto
        {
            get
            {
                return this._ValorPrevisto;
            }
            set
            {
                this._ValorPrevisto = value;
            }
        }
        public List<long> PlotXHistorico
        {
            get
            {
                return this._PlotXHistorico;
            }
            set
            {
                this._PlotXHistorico = value;
            }
        }
        public List<double> PlotYHistorico
        {
            get
            {
                return this._PlotYHistorico;
            }
            set
            {
                this._PlotYHistorico = value;
            }
        }
        public List<long> PlotXPrevisoes
        {
            get
            {
                return this._PlotXPrevisoes;
            }
            set
            {
                this._PlotXPrevisoes = value;
            }
        }
        public List<double> PlotYPrevisoes
        {
            get
            {
                return this._PlotYPrevisoes;
            }
            set
            {
                this._PlotYPrevisoes = value;
            }
        }
        public long IDEVENTO
        {
            get
            {
                return this._IDEVENTO;
            }
            set
            {
                this._IDEVENTO = value;
            }
        }
        public System.DateTime DATAHORAORIGEM
        {
            get
            {
                return this._DATAHORAORIGEM;
            }
            set
            {
                this._DATAHORAORIGEM = value;
            }
        }
        public System.Nullable<System.DateTime> DATAHORANORMORIGEM
        {
            get
            {
                return this._DATAHORANORMORIGEM;
            }
            set
            {
                this._DATAHORANORMORIGEM = value;
            }
        }
        public System.DateTime DATAHORAATUAL
        {
            get
            {
                return this._DATAHORAATUAL;
            }
            set
            {
                this._DATAHORAATUAL = value;
            }
        }
        public System.DateTime DATAHORAREPOSITORIO
        {
            get
            {
                return this._DATAHORAREPOSITORIO;
            }
            set
            {
                this._DATAHORAREPOSITORIO = value;
            }
        }
        public System.Nullable<System.DateTime> DATAHORANORMREPOSITORIO
        {
            get
            {
                return this._DATAHORANORMREPOSITORIO;
            }
            set
            {
                this._DATAHORANORMREPOSITORIO = value;
            }
        }
        public long OBJID
        {
            get
            {
                return this._OBJID;
            }
            set
            {
                this._OBJID = value;
            }
        }
        public string TAG
        {
            get
            {
                return this._TAG;
            }
            set
            {
                this._TAG = value;
            }
        }
        public string AREASEGENT
        {
            get
            {
                return this._AREASEGENT;
            }
            set
            {
                this._AREASEGENT = value;
            }
        }
        public string SINOTICO
        {
            get
            {
                return this._SINOTICO;
            }
            set
            {
                this._SINOTICO = value;
            }
        }
        public string GRUPO
        {
            get
            {
                return this._GRUPO;
            }
            set
            {
                this._GRUPO = value;
            }
        }
        public string CLASSE
        {
            get
            {
                return this._CLASSE;
            }
            set
            {
                this._CLASSE = value;
            }
        }
        public string DESCRICAO
        {
            get
            {
                return this._DESCRICAO;
            }
            set
            {
                this._DESCRICAO = value;
            }
        }
        public string TAGREM
        {
            get
            {
                return this._TAGREM;
            }
            set
            {
                this._TAGREM = value;
            }
        }
        public int PRIORIDADE
        {
            get
            {
                return this._PRIORIDADE;
            }
            set
            {
                this._PRIORIDADE = value;
            }
        }
        public string ESTADOALARME
        {
            get
            {
                return this._ESTADOALARME;
            }
            set
            {
                this._ESTADOALARME = value;
            }
        }
        public double VALORALARME
        {
            get
            {
                return this._VALORALARME;
            }
            set
            {
                this._VALORALARME = value;
            }
        }
        public string ESTADONORMALIZA
        {
            get
            {
                return this._ESTADONORMALIZA;
            }
            set
            {
                this._ESTADONORMALIZA = value;
            }
        }
        public System.Nullable<System.Double> VALORNORMALIZA
        {
            get
            {
                return this._VALORNORMALIZA;
            }
            set
            {
                this._VALORNORMALIZA = value;
            }
        }
        public System.Nullable<System.Int64> IDPTOVINC
        {
            get
            {
                return this._IDPTOVINC;
            }
            set
            {
                this._IDPTOVINC = value;
            }
        }
        public int ORIGEM
        {
            get
            {
                return this._ORIGEM;
            }
            set
            {
                this._ORIGEM = value;
            }
        }
        public int NORMALIZA
        {
            get
            {
                return this._NORMALIZA;
            }
            set
            {
                this._NORMALIZA = value;
            }
        }
        public int FAIXAALARME
        {
            get
            {
                return this._FAIXAALARME;
            }
            set
            {
                this._FAIXAALARME = value;
            }
        }
        public System.Nullable<System.Int32> FAIXANORMALIZA
        {
            get
            {
                return this._FAIXANORMALIZA;
            }
            set
            {
                this._FAIXANORMALIZA = value;
            }
        }
        public int BLINK
        {
            get
            {
                return this._BLINK;
            }
            set
            {
                this._BLINK = value;
            }
        }
        public int TIPOENTIDADE
        {
            get
            {
                return this._TIPOENTIDADE;
            }
            set
            {
                this._TIPOENTIDADE = value;
            }
        }
        public int TIPO
        {
            get
            {
                return this._TIPO;
            }
            set
            {
                this._TIPO = value;
            }
        }
        public virtual object Get(int fieldPos)
        {
            switch (fieldPos)
            {
                case 0: return this.DataHora;
                case 1: return this.Diferenca;
                case 2: return this.ValorAlarme;
                case 3: return this.ValorPrevisto;
                case 4: return this.PlotXHistorico;
                case 5: return this.PlotYHistorico;
                case 6: return this.PlotXPrevisoes;
                case 7: return this.PlotYPrevisoes;
                case 8: return this.IDEVENTO;
                case 9: return this.DATAHORAORIGEM;
                case 10: return this.DATAHORANORMORIGEM;
                case 11: return this.DATAHORAATUAL;
                case 12: return this.DATAHORAREPOSITORIO;
                case 13: return this.DATAHORANORMREPOSITORIO;
                case 14: return this.OBJID;
                case 15: return this.TAG;
                case 16: return this.AREASEGENT;
                case 17: return this.SINOTICO;
                case 18: return this.GRUPO;
                case 19: return this.CLASSE;
                case 20: return this.DESCRICAO;
                case 21: return this.TAGREM;
                case 22: return this.PRIORIDADE;
                case 23: return this.ESTADOALARME;
                case 24: return this.VALORALARME;
                case 25: return this.ESTADONORMALIZA;
                case 26: return this.VALORNORMALIZA;
                case 27: return this.IDPTOVINC;
                case 28: return this.ORIGEM;
                case 29: return this.NORMALIZA;
                case 30: return this.FAIXAALARME;
                case 31: return this.FAIXANORMALIZA;
                case 32: return this.BLINK;
                case 33: return this.TIPOENTIDADE;
                case 34: return this.TIPO;

                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Get()");
            };
        }
        public virtual void Put(int fieldPos, object fieldValue)
        {
            switch (fieldPos)
            {
                case 0: this.DataHora = (System.DateTime)fieldValue; break;
                case 1: this.Diferenca = (System.Double)fieldValue; break;
                case 2: this.ValorAlarme = (System.Double)fieldValue; break;
                case 3: this.ValorPrevisto = (System.Double)fieldValue; break;
                case 4: this.PlotXHistorico = (List<long>)fieldValue; break;
                case 5: this.PlotYHistorico = (List<double>)fieldValue; break;
                case 6: this.PlotXPrevisoes = (List<long>)fieldValue; break;
                case 7: this.PlotYPrevisoes = (List<double>)fieldValue; break;
                case 8: this.IDEVENTO = (System.Int64)fieldValue; break;
                case 9: this.DATAHORAORIGEM = (System.DateTime)fieldValue; break;
                case 10: this.DATAHORANORMORIGEM = (System.Nullable<System.DateTime>)fieldValue; break;
                case 11: this.DATAHORAATUAL = (System.DateTime)fieldValue; break;
                case 12: this.DATAHORAREPOSITORIO = (System.DateTime)fieldValue; break;
                case 13: this.DATAHORANORMREPOSITORIO = (System.Nullable<System.DateTime>)fieldValue; break;
                case 14: this.OBJID = (System.Int64)fieldValue; break;
                case 15: this.TAG = (System.String)fieldValue; break;
                case 16: this.AREASEGENT = (System.String)fieldValue; break;
                case 17: this.SINOTICO = (System.String)fieldValue; break;
                case 18: this.GRUPO = (System.String)fieldValue; break;
                case 19: this.CLASSE = (System.String)fieldValue; break;
                case 20: this.DESCRICAO = (System.String)fieldValue; break;
                case 21: this.TAGREM = (System.String)fieldValue; break;
                case 22: this.PRIORIDADE = (System.Int32)fieldValue; break;
                case 23: this.ESTADOALARME = (System.String)fieldValue; break;
                case 24: this.VALORALARME = (System.Double)fieldValue; break;
                case 25: this.ESTADONORMALIZA = (System.String)fieldValue; break;
                case 26: this.VALORNORMALIZA = (System.Nullable<System.Double>)fieldValue; break;
                case 27: this.IDPTOVINC = (System.Nullable<System.Int64>)fieldValue; break;
                case 28: this.ORIGEM = (System.Int32)fieldValue; break;
                case 29: this.NORMALIZA = (System.Int32)fieldValue; break;
                case 30: this.FAIXAALARME = (System.Int32)fieldValue; break;
                case 31: this.FAIXANORMALIZA = (System.Nullable<System.Int32>)fieldValue; break;
                case 32: this.BLINK = (System.Int32)fieldValue; break;
                case 33: this.TIPOENTIDADE = (System.Int32)fieldValue; break;
                case 34: this.TIPO = (System.Int32)fieldValue; break;
                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Put()");
            };
        }
    }
}
