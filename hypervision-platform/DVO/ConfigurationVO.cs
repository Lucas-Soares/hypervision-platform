﻿using hypervision_platform.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace hypervision_platform.DVO
{
    public class ConfigurationVO
    {
        public string Id { get; set; }
        public LogLevel MinLogLevel { get; set; }
        public string LogTopic { get; set; } // confirmar se faz sentido isso
        public string[] CardFolders { get; set; }
        public string KafkaServer { get; set; }
        public Dictionary<string, CategoryCardConfiguration> CategoryCardConfigurations { get; set; }
        public TimelineConfiguration TimelineConfigurations { get; set; }
        public EventFeedConfiguration EventFeedConfigurations { get; set; }
        public NavbarConfiguration NavbarConfigurations { get; set; }
    }
}
