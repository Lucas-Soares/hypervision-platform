using Commons_Core.Entities;
using hypervision_platform.Models.Enum;
using System;
using System.Collections.Generic;

namespace hypervision_platform.Models
{
    public partial class Card : Document
    {
        public string CardId { get; set; }
        public string Level { get; set; }
        public Severity Severity { get; set; }
        public DateTime Date { get; set; }
        public DateTime EventStart { get; set; }
        public DateTime? EventEnd { get; set; }
        public CardEvent Event { get; set; }
        public bool Dispached { get; set; }
        public bool Merged { get; set; }
        public string MergedCardId { get; set; }
        public EventType EventType { get; set; }
        public CardDetails CardDetails { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}