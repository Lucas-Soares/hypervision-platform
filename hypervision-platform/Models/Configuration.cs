﻿using Commons_Core.Entities;
using hypervision_platform.Models.Enum;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace hypervision_platform.Models
{
    public class Configuration : Document
    {
        public Configuration()
        {
        }

        public Configuration(LogLevel minLogLevel, string kafkaServer, Dictionary<string, CategoryCardConfiguration> categoryCardConfigurations,
                                TimelineConfiguration timelineConfigurations, EventFeedConfiguration eventFeedConfigurations, NavbarConfiguration navbarConfigurations)
        {
            MinLogLevel = minLogLevel;
            KafkaServer = kafkaServer;
            CategoryCardConfigurations = categoryCardConfigurations;
            TimelineConfigurations = timelineConfigurations;
            EventFeedConfigurations = eventFeedConfigurations;
            NavbarConfigurations = navbarConfigurations;
        }


        public void Update(LogLevel minLogLevel, string kafkaServer, Dictionary<string, CategoryCardConfiguration> categoryCardConfigurations,
                            TimelineConfiguration timelineConfigurations, EventFeedConfiguration eventFeedConfigurations, NavbarConfiguration navbarConfigurations)
        {
            MinLogLevel = minLogLevel;
            KafkaServer = kafkaServer;
            CategoryCardConfigurations = categoryCardConfigurations;
            TimelineConfigurations = timelineConfigurations;
            EventFeedConfigurations = eventFeedConfigurations;
            NavbarConfigurations = navbarConfigurations;
        }

        public LogLevel MinLogLevel { get; private set; }
        public string KafkaServer { get; private set; }
        public Dictionary<string, CategoryCardConfiguration> CategoryCardConfigurations { get; private set; }
        public TimelineConfiguration TimelineConfigurations { get; private set; }
        public EventFeedConfiguration EventFeedConfigurations { get; private set; }
        public NavbarConfiguration NavbarConfigurations { get; private set; }
    }

    public class CategoryCardConfiguration
    {
        public Severity Category { get; set; }
        public string NotificationSound { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }
        public bool RepeatNotificationSound { get; set; }
    }
}
