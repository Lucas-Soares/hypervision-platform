﻿using hypervision_platform.DVO;

namespace hypervision_platform.Models
{
    public class Converters
    {
        public static CardVO CardEntityToVO(Card card)
        {
            return new CardVO()
            {
                Id = card.CardId,
                Level = card.Level,
                Severity = card.Severity,
                Date = card.Date,
                EventStart = card.EventStart,
                EventEnd = card.EventEnd,
                LastEvent = card.Event,
                EventType = card.EventType,
                CardDetails = card.CardDetails,
                Dispached = card.Dispached,
                MergedCardId = card.MergedCardId
            };
        }

        public static Card CardVOToEntity(CardVO cardVO)
        {
            return new Card
            {
                CardId = cardVO.Id,
                CardDetails = cardVO.CardDetails,
                Date = cardVO.Date,
                Dispached = cardVO.Dispached,
                Event = cardVO.LastEvent,
                EventEnd = cardVO.EventEnd,
                EventStart = cardVO.EventStart,
                EventType = cardVO.EventType,
                Level = cardVO.Level,
                Severity = cardVO.Severity,
                MergedCardId = cardVO.MergedCardId
            };
        }
    }
}
