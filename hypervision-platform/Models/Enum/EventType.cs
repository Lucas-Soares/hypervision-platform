﻿namespace hypervision_platform.Models
{
    public enum EventType
    {
        Meteorological,
        Fire,
        TagMeasurement,
        RA,
        Fault
    }
}