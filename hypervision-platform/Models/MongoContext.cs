﻿using Commons_Core.Entities;
using System;

namespace hypervision_platform.Models
{
    public class MongoContext : BaseMongoContext
    {
        protected override void SetConfiguration()
        {
            ConnectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
            DataBaseName = Environment.GetEnvironmentVariable("DATABASE_NAME");
        }
    }
}
