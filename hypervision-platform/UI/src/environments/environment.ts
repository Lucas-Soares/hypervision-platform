// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hubUrl: "http://localhost:5001/eventHub",
  baseUrl: "http://localhost:5001/",
  authentication: "http://192.168.2.94:5000/",
  endPoints: {
    getEvents: "event/GetEvents/",
    acceptEvent: "event/AcceptEvent/",
    rejectEvent: "event/RejectEvent/",
    finishEvent: "event/FinishExpiredEvent/",
    deleteEvent: "event/DeleteEvent/",
    forwardEvent: "event/ForwardEvent/",
    registerUser: "event/RegisterUser/",
    mergeEvents: "event/MergeEvents/",
    getFields: "configuration/GetFields/",
    getUrls: "configuration/GetUrls/",
    getActiveUsers: "configuration/GetActiveUsers/",
    getConfiguration: "configuration/GetConfiguration/",
    saveConfiguration: "configuration/Save/",
    uploadFile: "configuration/OnPostUploadAsync/",
    getRDO: "event/GetRDO/",
    addActionOnEvent: "event/AddActionOnEvent/"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
