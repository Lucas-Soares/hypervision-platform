import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CommonService } from './common.service'

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private hubConnection: signalR.HubConnection;
  hubURL = environment.hubUrl;
  //public data: Card[] = [];

  constructor(private commonService: CommonService) { }

  public startConnection = (addCard: Function, finish: Function) => {
    if (!this.commonService.checkIfIsLogged()) {
      return;
    }

    this.hubConnection = new signalR.HubConnectionBuilder()
      // .withUrl(this.hubURL).build();
      .withUrl(this.hubURL, {
        accessTokenFactory: () => this.commonService.user.token
      })
      .configureLogging(signalR.LogLevel.Error)
      .build();
    this.hubConnection
      .start()
      .then((data) => {
        console.log('Connection started');
        finish(data);
        this.registerUser();
      })
      .catch(err => console.log('Error while starting connection: ' + err));

    this.hubConnection.on("ReceiveMessage", (message) => {
      addCard(message);
    });
  }

  public registerUser = () => {
    this.hubConnection.invoke("RegisterClient", this.commonService.user.user, this.commonService.user.filter.join(",")).catch(e => {
      console.error(e);
    })
      .then(d => {
        this.hubConnection.invoke("GetEvents", this.commonService.user.user);
      })

  };
}
