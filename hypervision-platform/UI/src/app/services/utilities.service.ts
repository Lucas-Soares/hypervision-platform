import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  getDataRandom(max = 100, min = 0): number {
    return Math.floor(Math.random() * max + min);
  }

  getData(quantity = 10, max = 100, min = 0): number[] {
    let data = [];
    for (let index = 0; index < quantity; index++) {
      data.push(this.getDataRandom(max, min));
    }
    return data;
  }

  getDownloadImgByStatus(tries, status): string{
    if (tries) {
      switch (status) {
        case 1:
          return '<style>'
              +  '@keyframes spin {'
              +  '  to { transform: rotate(360deg); }'
              +  '}'
              +  '.spinner {'
              +  '  border: 5px solid #BFCDD5A0;'
              +  '  border-left-color: #F7A30F;'
              +  '  border-radius: 50%;'
              +  '  width: 20px;'
              +  '  height: 20px;'
              +  '  animation: spin 2s linear infinite;'
              +  '}'
              +  '</style>'
              +  '<div title="Download em andamento" style="width: 80px; display: flex; justify-content: center;"><div class="spinner"></div></div>'

        case 2:
          return '<div title="Baixado" style="width: 80px; display: flex; justify-content: center; color: green;"><i class="fa fa-check"></i></div>';

        default:
          return '<div title="Falha" style="width: 80px; display: flex; justify-content: center; color: red;"><i class="fa fa-times"></i></div>';
      }
    } else {
      return '<div style="width: 80px; display: flex; justify-content: center;">-</div>'
    }
  }


  static cleanObject = (object: {}): any => {
    for (var propName in object) {
      if (object[propName] == null) delete object[propName];
    };
    return object;
  }

  static subtractArrays(array1: any[], array2: any[]) {
    return array1.filter(x => !array2.includes(x));
  }

  static intersectArrays(array1: any[], array2: any[]) {
    return array1.filter(x => array2.includes(x));
  }
}
