import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Feature } from '../models/Feature.model';
import { Token } from '../models/Token.model';
import { UtilitiesService } from './utilities.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public user: Token;
  baseUrlEndpoint = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
    console.log(this);
  }

  checkIfIsLogged(): boolean {
    let _ = sessionStorage.getItem("COD");
    if (!_) {
      window.location.href = "/login";
      return false;
    }
    let data = JSON.parse(_);
    this.user = {
      name: data.user.name,
      token: data.user.accessToken.token,
      user: data.user.login,
      filter: data.filter,
      modules: data.modules
    };

    let d = new Date();
    let expirationDate = new Date(data.user.accessToken.expirationDate);

    if(d > expirationDate){
      this.logout();
    }

    console.log(this.user.modules)
    return true;
  }
  setToken(d: any, filter: string[]) {
    d.filter = filter;
    sessionStorage.setItem("COD", JSON.stringify(d));
    window.location.href = "";
  }

  getAccesToken(): any {
    let _ = sessionStorage.getItem("COD");
    if (!_) {
      window.location.href = "/login";
      return false;
    }
    let data = JSON.parse(_);
    return data.user.accessToken;
  }

  logout(): boolean {
    this.user = undefined;
    sessionStorage.removeItem("COD");
    window.location.href = "/login";
    return true;
  }
  login(data: any) {
    const endpoint = environment.authentication;
    return this.httpClient.post(endpoint + "login", data);
  }
  userHasFeatureAccess(moduleName: string, featureName: string, permissionType: string) {
    let module = this.user?.modules?.find(x => x.name == moduleName);
    let feature = module?.features?.find(x => x.name == featureName);

    if (feature?.permission[permissionType]) {
      return true;
    }

    return false;
  }
  // recuperar áreas de atuação
  getFields(callback: (fields: string[]) => void) {
    let url = `${environment.endPoints.getFields}`;
    this.get(url).subscribe(data => {
      var areas = [];
      data.forEach(d => {
        areas.push(d);
      });
      callback(areas);
    }, err => {
      console.error(err);
    });
  }

  getActiveUsers(callback: (users: string[]) => void) {
    let url = `${environment.endPoints.getActiveUsers}`;
    this.get(url).subscribe(data => {
      callback(data);
    }, err => {
      console.error(err);
    });
  }

  getUsers(callback: (users: string[]) => void) {
    var listUsers = [];
    this.getAllFilter('Users', { orderBy: "Name", way: "ASC" }).subscribe(result => {
      result.dataList.forEach(element => {
        listUsers.push(element.login);
      });
      callback(listUsers);
    });    
  }

  getAllFilter(entity: string, filter: any, page = 0, listSize = 0): Observable<any> {
    const endpoint = environment.authentication;
    return this.httpClient.get(endpoint + entity + "/allFilter/" + page + "/" + listSize, { params: UtilitiesService.cleanObject(filter) });
  }

  getAll(entity: string): Observable<any> {
    return this.httpClient.post(this.baseUrlEndpoint + entity, {});
  }

  save(entity: string, fields: any): Observable<any> {
    return this.httpClient.post(this.baseUrlEndpoint + entity, fields);
  }

  update(entity: string, fields: any): Observable<any> {
    return this.httpClient.put(this.baseUrlEndpoint + entity, fields);
  }

  getOne(entity: string, id: string): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + entity + "/" + id);
  }

  get(entity: string): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + entity);
  }

  getModGroup(entity: string, idMod: string, IdGrp: string): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + entity + "/" + idMod + "/" + IdGrp);
  }

  getList(entity: string): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + entity);
  }

  getListWithParameter(entity: string, filter: any): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + entity, { params: UtilitiesService.cleanObject(filter) });
  }

  getConcessionsWithAssets(): Observable<any> {
    return this.httpClient.get(this.baseUrlEndpoint + "concessions/withAsset");
  }

  post(entity: string, data: any) {
    const endpoint = this.baseUrlEndpoint + entity;
    return this.httpClient.post(endpoint, data);
  }


  postFile(entity: string, fileToUpload: File): Observable<any> {
    const endpoint = this.baseUrlEndpoint + "/file";
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.httpClient.post(endpoint, formData);
  }

  uploadFile(entity: string, fileToUpload: File): Observable<any> {
    const endpoint = this.baseUrlEndpoint + entity;
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.httpClient.post(endpoint, formData, { reportProgress: true, observe: 'events' });
  }

  delete(entity: string): Observable<any> {
    return this.httpClient.delete(this.baseUrlEndpoint + entity);
  }

  downloadFile(entity: string, fileName: string): Observable<Blob> {
    return this.httpClient.get(this.baseUrlEndpoint + entity + "/" + fileName, { responseType: 'blob' });
  }
}
