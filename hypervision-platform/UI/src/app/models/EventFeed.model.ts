import { Event } from './Configuration.model';

interface EventFeed {
    feedBackgroundColor: string,
    cardBackgroundColor: string,
    dashboardBackgroundColor: string,
    fontColor: string,
    feedWidth: string,
    levels: Event[],
    displayDateTime: boolean,
    displayFilter: boolean,
    allowMultiselectionCard: boolean,
    soundAlert: boolean,
    allowHideCard: boolean
}

export { EventFeed };