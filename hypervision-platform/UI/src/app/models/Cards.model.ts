export enum UserActionType {
  None,
  Accept,
  Reject,
  Share,
  Comment,
  Finish,
  Expire,
  Start,
  Delete,
  Forward,
  Merge
}
