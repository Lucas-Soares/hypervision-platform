import { Feature } from './Feature.model';

interface Modules {
  name: string;
  features: Feature[];
}
export { Modules };
