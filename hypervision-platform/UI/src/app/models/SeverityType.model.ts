export enum SeverityType {
  INFO,
  NOTIFICATION,
  ALERT,
  ALARM
}
