import { Modules } from './Modules.model';

interface Token {
  user: string;
  token: string;
  name: string;
  filter: string[];
  modules: Modules[];
}
export { Token };
