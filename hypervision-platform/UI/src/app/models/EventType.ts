export enum EventType {
  Meteorological,
  Fire,
  TagMeasurement,
  RA,
  Fault
}
