interface FeaturePermissions {
  view: boolean;
  edit: boolean;
}
export { FeaturePermissions };
