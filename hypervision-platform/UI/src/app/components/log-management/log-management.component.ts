import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { LogLevel } from '@microsoft/signalr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-log-management',
  templateUrl: './log-management.component.html',
  styleUrls: ['./log-management.component.scss']
})
export class LogManagementComponent implements OnInit {

  configuration: any;
  logForm: FormGroup;
  submitted = false;
  modalRef: any;
  modalMessage: string;

  logLevels: any[] = [
    { type: LogLevel.Debug, value: "Debug" },
    { type: LogLevel.Information, value: "Informação" },
    { type: LogLevel.Warning, value: "Atenção" },
    { type: LogLevel.Error, value: "Erro" },
    { type: LogLevel.Critical, value: "Crítico" },
    { type: LogLevel.None, value: "Não logar" }
  ];

  @ViewChild('modalConfirm') private modalConfirm: TemplateRef<any>;

  constructor(
    private title: Title,
    private services: CommonService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Plataforma Hypervision");
    this.services.checkIfIsLogged();
    this.getConfiguration();
  }

  getConfiguration(){
    this.services.get(environment.endPoints.getConfiguration).subscribe(d => {
      
      this.configuration = d;

      this.initializeForm();
    }, err => {
      console.error(err);
    });
  }

  initializeForm() {
    this.logForm = this.fb.group({
      minLogLevel: [this.configuration.minLogLevel],
    });
  }

  openModal(): void {
    this.modalMessage = "Salvar alterações?";
    this.modalRef = this.modalService.open(this.modalConfirm);
  }

  saveConfirm() { 
    this.submitted = true;
    if (this.logForm.invalid) {
      this.modalRef.close();
      return;
    }

    this.configuration.minLogLevel = this.logForm.value.minLogLevel;

    this.modalRef.close();

    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => { console.log('Operação realizada com sucesso!') },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => { 
      });
  }
}
