import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NavbarManagementComponent } from './navbar-management.component';

const routes: Routes = [{ path: '', component: NavbarManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavbarManagementRoutingModule { }
