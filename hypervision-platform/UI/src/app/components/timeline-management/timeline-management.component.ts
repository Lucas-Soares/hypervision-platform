import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Configuration } from '../../models/Configuration.model';

@Component({
  selector: 'app-timeline-management',
  templateUrl: './timeline-management.component.html',
  styleUrls: ['./timeline-management.component.scss']
})
export class TimelineManagementComponent implements OnInit {
  standardFonts: string[] = ["Roboto", "Helvetica Neue", "sans-serif"];

  configForm: FormGroup;
  configuration: Configuration;
  timelineConfig: any;
  modalRef: any;
  modalMessage = "Salvar alterações?";

  @ViewChild('modalConfirm') private modalConfirm: TemplateRef<any>;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private services: CommonService
  ) { }

  ngOnInit(): void {
    this.getConfig();
  }

  initializeConfig() {
    let defaultColor = "#f1f1f1";
    this.timelineConfig = {
      currentTimeColor: defaultColor,
      fontColor: defaultColor,
      fontFamily: this.standardFonts[0],
      fontSize: 12,
      graphBackgroundColor: defaultColor,
      resumeBackgroundColor: defaultColor,
      heigth: 150,
      initialLimit: 1,
      finalLimit: 3
    };

  }

  openModal(): void {
    this.modalRef = this.modalService.open(this.modalConfirm);
  }

  getConfig() {
    this.services.get(environment.endPoints.getConfiguration).subscribe(d => {
      this.configuration = d;

      if (this.configuration)
        if (this.configuration.timelineConfigurations)
          this.timelineConfig = this.configuration.timelineConfigurations;
        else
          this.initializeConfig();
      else {
        this.initializeConfig();
        this.configuration = {
          timelineConfigurations: this.timelineConfig
        }
      }
      this.initializeForm();
    }, err => {
      console.error(err);
    });
  }

  initializeForm() {
    this.configForm = this.fb.group({
      resumeBackgroundColor: [this.timelineConfig == undefined ? null : this.timelineConfig.resumeBackgroundColor, Validators.required],
      graphBackgroundColor: [this.timelineConfig == undefined ? null : this.timelineConfig.graphBackgroundColor, Validators.required],
      currentTimeColor: [this.timelineConfig == undefined ? null : this.timelineConfig.currentTimeColor, Validators.required],
      fontFamily: [this.timelineConfig == undefined ? null : this.timelineConfig.fontFamily, Validators.required],
      fontColor: [this.timelineConfig == undefined ? null : this.timelineConfig.fontColor, Validators.required],
      fontSize: [this.timelineConfig == undefined ? null : this.timelineConfig.fontSize, Validators.required],
      heigth: [this.timelineConfig == undefined ? null : this.timelineConfig.heigth, Validators.required],
      scale: [this.timelineConfig == undefined ? null : (this.timelineConfig.initialLimit + this.timelineConfig.finalLimit), Validators.required]
    });
  }

  saveConfirm() {
    // if (!this.configForm.valid) return;

    this.timelineConfig = {
      currentTimeColor: this.configForm.value.currentTimeColor,
      fontColor: this.configForm.value.fontColor,
      fontSize: this.configForm.value.fontSize,
      fontFamily: this.configForm.value.fontFamily,
      graphBackgroundColor: this.configForm.value.graphBackgroundColor,
      resumeBackgroundColor: this.configForm.value.resumeBackgroundColor,
      heigth: this.configForm.value.heigth,
      initialLimit: 1,
      finalLimit: this.configForm.value.scale - 1
    };

    this.configuration.timelineConfigurations = this.timelineConfig;

    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => {
        console.log('Operação realizada com sucesso!')
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => {
        this.modalRef.close();
      }
      );
  }

}
