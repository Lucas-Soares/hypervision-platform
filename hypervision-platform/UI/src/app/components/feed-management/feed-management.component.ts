import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { SeverityType } from '../../models/SeverityType.model';
import { Event } from '../../models/Configuration.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-feed-management',
  templateUrl: './feed-management.component.html',
  styleUrls: ['./feed-management.component.scss']
})
export class FeedManagementComponent implements OnInit {
  levels: Event[];

  configForm: FormGroup;
  configuration: any;
  eventFeedConfig: any;
  modalRef: any;
  modalMessage = "Salvar alterações?";

  severityTypes: any[] = [
    { type: SeverityType.INFO, value: "Informação" },
    { type: SeverityType.NOTIFICATION, value: "Notificação" },
    { type: SeverityType.ALERT, value: "Alerta" },
    { type: SeverityType.ALARM, value: "Alarme" },
  ];

  @ViewChild('modalConfirm') private modalConfirm: TemplateRef<any>;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private services: CommonService
  ) { }

  ngOnInit(): void {
    this.getConfig();
  }

  initializeConfig() {
    let defaultColor = "#f1f1f1";
    this.eventFeedConfig = {
      feedBackgroundColor: defaultColor,
      cardBackgroundColor: defaultColor,
      dashboardBackgroundColor: defaultColor,
      fontColor: defaultColor,
      feedWidth: "29%",
      // levels: this.levels[0],
      displayDateTime: false,
      displayFilter: false,
      allowMultiselectionCard: false,
      soundAlert: false,
      allowHideCard: false,
      orderBySeverity: false
    };

  }

  openModal(): void {
    this.modalRef = this.modalService.open(this.modalConfirm);
  }

  getConfig() {
    this.services.get(environment.endPoints.getConfiguration).subscribe(d => {
      this.configuration = d;

      if(this.configuration)
        if(this.configuration.eventFeedConfigurations)
          this.eventFeedConfig = this.configuration.eventFeedConfigurations;
        else
          this.initializeConfig();
      else{
        this.initializeConfig();
        this.configuration = {
          eventFeedConfigurations: this.eventFeedConfig
        }
      }

      // if(this.configuration.categoryCardConfigurations)
      //   this.getLevels();

      this.initializeForm();
    }, err => {
      console.error(err);
    });
  }

  initializeForm() {
    this.configForm = this.fb.group({
      feedBackgroundColor: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.feedBackgroundColor, Validators.required],
      cardBackgroundColor: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.cardBackgroundColor, Validators.required],
      dashboardBackgroundColor: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.dashboardBackgroundColor, Validators.required],
      fontColor: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.fontColor, Validators.required],
      // levels: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.levels, Validators.required],
      feedWidth: [this.eventFeedConfig == undefined ? null : this.eventFeedConfig.feedWidth.replace(/[^0-9]/g,''), Validators.required],
      displayDateTime: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.displayDateTime, Validators.required],
      displayFilter: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.displayFilter, Validators.required],
      allowMultiselectionCard: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.allowMultiselectionCard, Validators.required],
      soundAlert: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.soundAlert, Validators.required],
      allowHideCard: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.allowHideCard, Validators.required],
      orderBySeverity: [this.eventFeedConfig == undefined ? false : this.eventFeedConfig.orderBySeverity, Validators.required],
    });
  }

  saveConfirm() {
    // if (!this.configForm.valid) return;

    this.eventFeedConfig = {
      feedBackgroundColor: this.configForm.value.feedBackgroundColor,
      cardBackgroundColor: this.configForm.value.cardBackgroundColor,
      dashboardBackgroundColor: this.configForm.value.dashboardBackgroundColor,
      fontColor: this.configForm.value.fontColor,
      // levels: this.configForm.value.levels,
      feedWidth: this.configForm.value.feedWidth.toString() + "%",
      displayDateTime: this.configForm.value.displayDateTime,
      displayFilter: this.configForm.value.displayFilter,
      allowMultiselectionCard: this.configForm.value.allowMultiselectionCard,
      soundAlert: this.configForm.value.soundAlert,
      allowHideCard: this.configForm.value.allowHideCard,
      orderBySeverity: this.configForm.value.orderBySeverity
    };

    this.configuration.eventFeedConfigurations = this.eventFeedConfig;
    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => {
        console.log('Operação realizada com sucesso!')
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => {
        this.modalRef.close();
      }
    );
  }

  getLevels() {
    this.levels = Object.values(this.configuration.categoryCardConfigurations);

    this.levels = this.levels.map(item => {
      return {
        name: item.name ? item.name : this.severityTypes.find(m => m.type == item.category).value,
        color: item.color,
        category: item.category,
        categoryName: this.severityTypes.find(m => m.type == item.category).value,
        notificationSound: item.notificationSound,
        repeatNotificationSound: item.repeatNotificationSound
      }
    });
  }

}
