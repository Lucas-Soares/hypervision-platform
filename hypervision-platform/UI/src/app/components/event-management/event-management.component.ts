import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Event } from '../../models/Configuration.model';
import { SeverityType } from '../../models/SeverityType.model';
import { ConcertTable } from '@xst/concert-table-io';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-event-management',
  templateUrl: './event-management.component.html',
  styleUrls: ['./event-management.component.scss']
})
export class EventManagementComponent implements OnInit {
  urls = [];

  editEventData: Event = { name: '', categoryName: null, category: null, notificationSound: '', color: '', repeatNotificationSound: false };
  editEvent = false;
  configuration: any;
  listEvents: Event[];
  eventForm: FormGroup;
  submitted = false;
  modalMessage: string;
  modalRef: any;
  tableConfig: ConcertTable = {
    registrationAmount: 0,
    hideColumnSelectAll: true,
    column: [],
    attributeName: [],
    data: [],
    actionList: [],
    actionListTop: [],
  };
  removeEvent = false;
  eventToRemove: Event;
  audioFilePath: string;

  severityTypes: any[] = [
    { type: SeverityType.INFO, value: "Informação" },
    { type: SeverityType.NOTIFICATION, value: "Notificação" },
    { type: SeverityType.ALERT, value: "Alerta" },
    { type: SeverityType.ALARM, value: "Alarme" },
  ];

  @ViewChild('modalConfirm') private modalConfirm: TemplateRef<any>;

  constructor(private title: Title,
    private services: CommonService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.title.setTitle("Plataforma Hypervision");
    this.services.checkIfIsLogged();
    this.getEvents();
    this.initializeForm();
  }

  get f() { return this.eventForm.controls; }

  getEvents() {
    this.services.get(environment.endPoints.getConfiguration).subscribe(d => {
      this.configuration = d;

      this.getListEvents();

      this.initializeTable();
    }, err => {
      console.error(err);
    });
  }

  initializeTable() {
    this.tableConfig.data = this.listEvents;
    this.tableConfig.column = this.getColumns();
    this.tableConfig.attributeName = this.getAttributeColumnsNames();
    this.tableConfig.actionList = this.getActionList();
    this.tableConfig.actionListTop = this.getActionListTop();
    this.tableConfig.registrationAmount = this.listEvents?.length;

    this.updateTableData();
  }


  getColumns() {
    return [
      { name: 'Nome' },
      { name: 'Categoria' },
      { name: 'Sinal Sonoro' },
      { name: 'Ações',  width: '200px' }
    ];
  }

  getAttributeColumnsNames() {
    return [
      { name: 'name' },
      { name: 'categoryName' },
      { name: 'notificationSound' },
    ];
  }

  getActionList() {
    return [
      {
        name: "Editar",
        class: "btn btn-outline-primary btn-sm mr-2",
        icon: "fa fa-edit",
        callback: (data) => {
          this.modalMessage = "Salvar alterações?";
          this.editEvent = true;
          this.editEventData = data;
          this.initializeForm();
        }
      },
      {
        name: "Remover",
        class: "btn btn-outline-laranja-4 btn-sm mr-2",
        icon: "fa fa-trash",
        callback: (data) => {
          this.eventToRemove = data;
          this.modalMessage = "Excluir evento selecionado?";
          this.removeEvent = false;
          this.removeEvent = true;
          this.openModal();
          
          this.updateTableData();
        }
      }
    ]
  }

  getActionListTop() {
    return [
      {
        name: "Novo",
        class: "btn btn-info add-user-button btn-sm mr-2",
        icon: "fa fa-plus",
        callback: (data) => {
          this.modalMessage = "Adicionar Evento?"
          this.editEventData = { name: '', categoryName: null, category: null, notificationSound: '', color: '', repeatNotificationSound: false };
          this.initializeForm();
          this.removeEvent = false;
          this.editEvent = true;
        }
      }
    ];
  }

  initializeForm() {
    let eventType: any;
    let fileName = "";

    if (this.editEventData.category != null) {
      eventType = this.severityTypes.find(m => m.type == this.editEventData.category);
    }
    if (this.editEventData.notificationSound){
      fileName = this.extractFileName(this.editEventData.notificationSound);
      document.getElementById('fileSoundLabel').innerHTML = fileName;
    }

    this.eventForm = this.fb.group({
      name: [this.editEventData.name],
      category: [eventType ? eventType.type : null, [Validators.required]],
      color: [this.editEventData.color],
      notificationSound: [this.editEventData.notificationSound],
      repeatNotificationSound: [this.editEventData.repeatNotificationSound],
    });

  }

  getListEvents() {
    if (this.configuration.categoryCardConfigurations == null)
      return this.listEvents = [];

    this.listEvents = Object.values(this.configuration.categoryCardConfigurations);

    this.listEvents = this.listEvents.map(item => {
      return {
        name: item.name ? item.name : this.severityTypes.find(m => m.type == item.category).value,
        color: item.color,
        category: item.category,
        categoryName: this.severityTypes.find(m => m.type == item.category).value,
        notificationSound: this.extractFileName(item.notificationSound),
        repeatNotificationSound: item.repeatNotificationSound
      }
    });
  }

  openModal(): void {
    this.modalRef = this.modalService.open(this.modalConfirm);
  }

  closeEdit(): void {
    this.editEvent = false;
    this.submitted = false;
    document.getElementById('fileSoundLabel').innerHTML = null;
  }

  confirm() {
    if(this.editEvent)
      this.saveConfirm();
    else 
      this.removeConfirm();
  }

  updateTableData() {
    this.tableConfig = Object.assign({}, this.tableConfig);
  }

  removeConfirm() {
    delete this.configuration.categoryCardConfigurations[SeverityType[this.eventToRemove.category]];

    this.modalRef.close();
    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => { 
        console.log('Operação realizada com sucesso!');
        this.tableConfig.data = this.listEvents;
        this.getEvents();
        this.updateTableData(); 
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => { 
        this.removeEvent = false;
      });
  }

  saveConfirm() { 
    this.submitted = true;
    if (this.eventForm.invalid) {
      this.modalRef.close();
      return;
    }

    var objData = {
      name: this.eventForm.value.name,
      category: this.eventForm.value.category,
      color: this.eventForm.value.color,
      notificationSound: this.audioFilePath ? this.audioFilePath : this.editEventData.notificationSound,
      repeatNotificationSound: this.eventForm.value.repeatNotificationSound,
    }

    var eventCategory = SeverityType[this.eventForm.value.category].toUpperCase();

    if (this.configuration.categoryCardConfigurations == null) {
      this.configuration.categoryCardConfigurations = {  };
    }

    this.configuration.categoryCardConfigurations[eventCategory] = objData;

    this.modalRef.close();
    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => { 
        console.log('Operação realizada com sucesso!') 
        this.getListEvents();
        this.tableConfig.data = this.listEvents;
        this.updateTableData(); 
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => { 
        this.closeEdit();
      });
  }

  onChange(event){
    var selectedFile = <FileList>event.srcElement.files;
    document.getElementById('fileSoundLabel').innerHTML = selectedFile[0].name;
    
    var file = selectedFile[0];
    var url = `${environment.endPoints.uploadFile}`;

    this.services.uploadFile(url, file).subscribe(
      //try
      response => { 
        this.audioFilePath = response.body;
        document.getElementById('fileSoundLabel').innerHTML = selectedFile[0].name;
        console.log('Operação realizada com sucesso!') 
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => { 
      });
  }

  extractFileName(path) {
    path = path.replace(/\\/g, '/');
    var file = path.substring(path.lastIndexOf('/') + 1);
    
	  return file;
  } 

  receiveFeedback(data) {
    console.log(data);
  }
}
