import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'rdo/:eventId',
    loadChildren: () => import('./views/fire-rdo/fire-rdo.module').then(m => m.FireRDOModule)
  },
  {
    path: 'measurement/:eventId',
    loadChildren: () => import('./views/measurement/measurement.module').then(m => m.MeasurementModule)
  },
  {
    path: 'fault/:eventId',
    loadChildren: () => import('./views/fault-rdo/fault-rdo.module').then(m => m.FaultRDOModule)
  },
  {
    path: 'rardo/:eventId',
    loadChildren: () => import('./views/ra-rdo/ra-rdo.module').then(m => m.RARDOModule)
  },
  {
    path: 'raalarmsrdo/:eventId',
    loadChildren: () => import('./views/ra-alarms-rdo/ra-alarms-rdo.module').then(m => m.RAAlarmsRDOModule)
  },
  {
    path: 'configuration',
    loadChildren: () => import('./views/configuration/configuration.module').then(m => m.ConfigurationModule)
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponents = [];
