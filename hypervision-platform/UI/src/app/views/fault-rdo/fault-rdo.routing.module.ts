import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaultRdoComponent } from './fault-rdo.component';

const routes: Routes = [
    {
        path: '',
        component: FaultRdoComponent,
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaultRdoRoutingModule {}
