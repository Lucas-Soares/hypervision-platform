import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConcertLoadingIoService } from '@xst/concert-loading-io';
import { UserActionType } from 'src/app/models/Cards.model';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fault-rdo',
  templateUrl: './fault-rdo.component.html',
  styleUrls: ['./fault-rdo.component.scss']
})
export class FaultRdoComponent implements OnInit {
  eventId: string;
  events: any;
  lastEvent: any;
  comment: string = "";
  modalTitle: string = "";
  modalButton: string = "";
  currentUser: string = "";
  openEquipments: [];
  openProtections: any[];

  @ViewChild('modal') private modalReject: TemplateRef<any>;


  constructor(
    private services: CommonService,
    private route: ActivatedRoute,
    private loadingService: ConcertLoadingIoService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.services.checkIfIsLogged();
    this.currentUser = this.services.user.user;
    this.eventId = this.route.snapshot.paramMap.get('eventId');
    if (this.services.userHasFeatureAccess("EventHypervision", "GetRDO", "view")) {
      this.getEvents();
    }
  }

  getEvents() {
    this.loadingService.show();
    let url = `${environment.endPoints.getRDO}4/${this.eventId}`;
    this.services.get(url).subscribe(data => {
      this.events = data;
      this.lastEvent = this.events[this.events.length - 1];
      this.openEquipments = this.lastEvent.cardDetails.tag.split(',').map(t => { return t.replace("_ED", "") });
      this.openProtections = [];
      this.lastEvent.cardDetails.properties.forEach(p => {
        let isProtection = p.find(_ => { return _.name == "description" && _.value.indexOf("RELE") > -1 });
        if (isProtection) {
          let tag = p.find(_ => { return _.name == "tag" }).value.replace("_ED", "");
          if (this.openProtections.indexOf(tag) < 0) {
            this.openProtections.push(tag);
          }
        }

      });
      this.loadingService.hide();
    }, err => {
      this.loadingService.hide();
      console.error(err);
    });

  }

  getField(line: any, type: string) {
    var field = line.find(l => l.field == type);
    if (field) {
      return field.value;
    } else {
      debugger;
      return "";
    }
  }

  addComment(title: string) {
    this.modalTitle = "Incluir ação";
    this.modalButton = "Incluir";
    var modalRef = this.modalService.open(this.modalReject);
    modalRef.result.then((result) => {
      this.loadingService.show();
      let url = `${environment.endPoints.addActionOnEvent}4/${this.services.user.user}/${this.comment}`;
      this.services.update(url, [this.lastEvent]).subscribe(data => {
        this.loadingService.hide();
        this.getEvents();
        this.comment = "";
      }, err => {
        console.error(err);
      });
    }, err => {
      console.error(err);
    });
  }

  finishEvent() {
    let comment = "Evento finalizado pelo usuário " + this.services.user.user;
    let url = `${environment.endPoints.addActionOnEvent}5/${this.services.user.user}/${comment}`;
    this.services.update(url, [this.lastEvent]).subscribe(data => {
      this.loadingService.hide();
      this.getEvents();
      this.comment = "";
    }, err => {
      console.error(err);
    });
  }

  userHasPermission(moduleName: string, featureName: string, permissionType: string) {

    if (this.services.userHasFeatureAccess(moduleName, featureName, permissionType)) {
      return true;
    }

    return false;
  }

  validateComment() {
    return this.comment.match('^([a-zA-Z\u00C0-\u017F´\\d_\\-])[a-zA-Z\u00C0-\u017F´\\s_\\-\\d\\D]{2,}$') ? false : true;
  }

  showButton(type: string): boolean {
    if (this.lastEvent.lastEvent.type == UserActionType.Forward || this.lastEvent.lastEvent.type == UserActionType.Finish) return false;

    switch (type) {
      case 'addAction':
      case 'finishEvent': return this.lastEvent.lastEvent.user == this.currentUser;
      case 'none': return this.lastEvent.lastEvent.type == UserActionType.None;
    }

  }

}
