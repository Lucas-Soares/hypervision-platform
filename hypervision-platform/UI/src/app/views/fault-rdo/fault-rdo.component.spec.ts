import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaultRdoComponent } from './fault-rdo.component';

describe('FaultRdoComponent', () => {
  let component: FaultRdoComponent;
  let fixture: ComponentFixture<FaultRdoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaultRdoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaultRdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
