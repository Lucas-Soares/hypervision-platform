import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login.routing.module';
import { ConcertLoginIoModule } from '@xst/concert-login-io';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ConcertLoginIoModule,
    ConcertLoadingIoModule
  ]
})
export class LoginModule { }
