import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeasurementComponent } from './measurement.component';
import { MeasurementRoutingModule } from './measurement.routing.module';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular-highcharts';

@NgModule({
  declarations: [
    MeasurementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MeasurementRoutingModule,
    ConcertLoadingIoModule,
    ChartModule 
  ]
})
export class MeasurementModule { }
