import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RARdoComponent } from './ra-rdo.component';

describe('RARdoComponent', () => {
  let component: RARdoComponent;
  let fixture: ComponentFixture<RARdoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RARdoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RARdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
