import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RARdoComponent } from './ra-rdo.component';
import { RARDORoutingModule } from './ra-rdo.routing.module';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RARdoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RARDORoutingModule,
    ConcertLoadingIoModule
  ]
})
export class RARDOModule { }
