import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FireRdoComponent } from './fire-rdo.component';

describe('FireRdoComponent', () => {
  let component: FireRdoComponent;
  let fixture: ComponentFixture<FireRdoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FireRdoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FireRdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
