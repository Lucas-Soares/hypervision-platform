import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FireRdoComponent } from './fire-rdo.component';
import { FireRDORoutingModule } from './fire-rdo.routing.module';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FireRdoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FireRDORoutingModule,
    ConcertLoadingIoModule
  ]
})
export class FireRDOModule { }
