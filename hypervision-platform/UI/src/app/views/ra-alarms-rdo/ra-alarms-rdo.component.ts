import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { ConcertLoadingIoService } from '@xst/concert-loading-io';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventType } from 'src/app/models/EventType';
import { ActionType } from '@xst/concert-cardfeed-io';
import { UserActionType } from 'src/app/models/Cards.model';
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-ra-alarms-rdo',
  templateUrl: './ra-alarms-rdo.component.html',
  styleUrls: ['./ra-alarms-rdo.component.scss']
})
export class RAAlarmsRdoComponent implements OnInit {
  eventId: string;
  events: any;
  lastEvent: any;
  cycle: string;
  status: string;
  columns: string[] = [];
 
  comment: string = "";
  modalTitle: string = "";
  modalButton: string = "";
  currentUser: string = "";

  @ViewChild('modal') private modalReject: TemplateRef<any>;

  constructor(
    private services: CommonService,
    private route: ActivatedRoute,
    private loadingService: ConcertLoadingIoService,
    private modalService: NgbModal,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.services.checkIfIsLogged();
    this.currentUser = this.services.user.user;
    this.eventId = this.route.snapshot.paramMap.get('eventId');
    if (this.services.userHasFeatureAccess("EventHypervision", "GetRDO", "view")) {
      this.getEvents();
    }
  }

  getEvents() {
    this.loadingService.show();
    let url = `${environment.endPoints.getRDO}3/${this.eventId}`;
    this.services.get(url).subscribe(data => {
      this.events = data;
      // if(data[data.length -1].lastEvent.type == UserActionType.Forward){
      //   this.events = data;
      // }else{
      //   this.events = data.filter(e => {
      //     return this.services.user.user == e.lastEvent.user || e.lastEvent.user == null;
      //   });
      // }
      this.lastEvent = this.events[this.events.length - 1];
      this.removeId();
      this.getDates();
      this.getColumns();
      this.setStatus(event);
      this.loadingService.hide();
    }, err => {
      this.loadingService.hide();
      console.error(err);
    });

  }

  getColumns() {
    this.lastEvent.cardDetails.properties[0].forEach((prop, index) => {
      this.columns[index] = prop.label;
    });
  }

  getDates() {
    this.lastEvent.cardDetails.properties.forEach((element) => {
      element.forEach(prop => {
        if(prop.label.includes("Data ") && prop.value){
          var temp = prop.value.split('/');
          var date = (Date.parse(temp[1] + "/" + temp[0] + "/" + temp[2]) - (3 * 3600000));
          prop.value = this.datepipe.transform(date, 'dd/MM/yyyy HH:mm:ss');
        }        
      });
    })
  }

  removeId() {
    this.lastEvent.cardDetails.properties.forEach((prop) => {
      prop.splice(prop.length-1, 1);
    });
  }

  setStatus(event) {
    var fullStatus = this.lastEvent.cardDetails.status;
    if (fullStatus.endsWith("NS")) {
      this.status = "Não satisfatório";
      this.cycle = fullStatus.substring(0, fullStatus.length - 2);
    } else if (fullStatus.endsWith("SN")) {
      this.status = "Não satisfatório";
      this.cycle = fullStatus.substring(0, fullStatus.length - 2);
    } else if (fullStatus.endsWith("S")) {
      this.status = "Satisfatório";
      this.cycle = fullStatus.substring(0, fullStatus.length - 1);
    } else {
      this.status = "Em andamento";
      this.cycle = fullStatus;
    }
  }
    

}
