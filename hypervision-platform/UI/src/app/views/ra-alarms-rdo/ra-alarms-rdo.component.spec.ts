import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RAAlarmsRdoComponent } from './ra-alarms-rdo.component';

describe('RAAlarmsRdoComponent', () => {
  let component: RAAlarmsRdoComponent;
  let fixture: ComponentFixture<RAAlarmsRdoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RAAlarmsRdoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RAAlarmsRdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
