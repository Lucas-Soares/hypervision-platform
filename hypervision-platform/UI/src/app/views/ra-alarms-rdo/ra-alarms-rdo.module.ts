import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RAAlarmsRdoComponent } from './ra-alarms-rdo.component';
import { RAAlarmsRDORoutingModule } from './ra-alarms-rdo.routing.module';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    RAAlarmsRdoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RAAlarmsRDORoutingModule,
    ConcertLoadingIoModule
  ],
  providers: [DatePipe]
})
export class RAAlarmsRDOModule { }
