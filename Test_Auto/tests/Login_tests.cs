using OpenQA.Selenium;
using System;
using Test_Auto.pages;
using Test_Auto.utils;
using NUnit.Framework;
namespace Test_Auto.tests
{
    public class Login_tests : WebDriverConfig
    {
        //variables
        IWebDriver driver;
        Login_POM loginTest;
        NavBar_POM navBarTest;

        //variables - For to do Login
        string userNameTest = "user.test";
        string passowordTest = "Abc123";
        string myChoiceCustomFilter = "Selecionar Todos";
        bool passed = false;

        [SetUp]
        public void OpenChrome()
        {
            try
            {
                driver = WebDriverInitialConfig();
                driver.Navigate().GoToUrl("http://192.168.2.94:5001/login");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        [Test]
        public void T1_ItemsOnScreen()
        {
            //This test will verify that all items are on the screen
            try
            {
                loginTest = new Login_POM(driver);
                Assert.True(loginTest.OnLoginScreen());
                /*Assert.True(loginTest.ImgOverTitle.Displayed);
                Assert.That(loginTest.TitleLogin.Text, Is.EqualTo("Plataforma Hypervision"));
                Assert.True(loginTest.InputLogin.Displayed);
                Assert.True(loginTest.InputPassword.Displayed);
                Assert.True(loginTest.BtnShowPassword.Displayed);
                Assert.True(loginTest.CustomFilter.Displayed);
                Assert.True(loginTest.BtnLogin.Displayed);*/
            }
            catch (AssertionException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        [Test]
        public void T2_TryDoLoginHappyWay()
        {/*this test will try to do login on the way happy*/
            try
            {
                loginTest = new Login_POM(driver);
                loginTest.DoLogin(userNameTest, passowordTest, myChoiceCustomFilter);
                navBarTest = new NavBar_POM(driver);
                passed = (navBarTest.TitleNavBar.Text == "Plataforma Hypervision");
                //Console.WriteLine(passed);
                Assert.IsTrue(passed);
            }
            catch (AssertionException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        [Test]
        public void T3_TryDoLoginWrongUser()
        {/*this test will try to do login on the wrong user  */
            try
            {
                loginTest = new Login_POM(driver);
                loginTest.DoLogin("X", passowordTest, myChoiceCustomFilter);
                passed = (loginTest.MsgLogFail.Text == "Conexão falhou!");
                Assert.IsTrue(passed);
            }
            catch (AssertionException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        [Test]
        public void T4_TryDoLoginWrongPassword()
        {/*this test will try to do login on the wrong password*/
            try
            {
                loginTest = new Login_POM(driver);
                loginTest.DoLogin(userNameTest, "x", myChoiceCustomFilter);
                passed = (loginTest.MsgLogFail.Text == "Conexão falhou!");
                Assert.IsTrue(passed);
            }
            catch (AssertionException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        [TearDown]
        public void CloseChrome()
        {
            WebDriverClose(driver);
        }
    }
}