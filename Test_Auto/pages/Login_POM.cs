using OpenQA.Selenium;
using System.Collections.Generic;

namespace Test_Auto.pages
{
    public class Login_POM : Pages
    {
        public Login_POM(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        //Items of Login
        public IWebElement ImgOverTitle { get => driver.FindElement(By.CssSelector("#formContent > div:nth-child(1) > img")); }
        public IWebElement TitleLogin { get => driver.FindElement(By.CssSelector("#formContent > div.titulo-login.text-center")); }
        public IWebElement InputLogin { get => driver.FindElement(By.Id("iptUserName")); }
        public IWebElement InputPassword { get => driver.FindElement(By.Id("iptPassword")); }
        public IWebElement BtnLogin { get => driver.FindElement(By.Id("btnLogin")); }
        public IWebElement BtnShowPassword { get => driver.FindElement(By.Id("showPassword")); }
        public IWebElement MsgLogFail { get => driver.FindElement(By.CssSelector("#formContent > div:nth-child(3) > div > form > div.alert.alert-danger > div")); }

        //Will capture all items within the select
        public IWebElement CustomFilter { get => driver.FindElement(By.Id("loginFilter")); }

        //this method will insert the name, password and the item of the Custom Filter you choose
        public void DoLogin(string login, string password, string myChoice)
        {
            string lookForThisItem = "option";
            InputLogin.Clear();
            InputPassword.Clear();
            InputLogin.SendKeys(login);
            InputPassword.SendKeys(password);
            List<IWebElement> result = CreateListElements(CustomFilter, lookForThisItem);
            ChooseInSelect(myChoice, result);
            BtnLogin.Click();
        }
        public void DoLogin() // depois alterar estes usuário, ele está o mesmo da outra classe!!!!
        {
            //variables - For to do Login
            string userNameTest = "user.test";
            string passowordTest = "Abc123";
            string myChoiceCustomFilter = "Selecionar Todos";
            string lookForThisItem = "option";
            InputLogin.Clear();
            InputPassword.Clear();
            InputLogin.SendKeys(userNameTest);
            InputPassword.SendKeys(passowordTest);
            List<IWebElement> result = CreateListElements(CustomFilter, lookForThisItem);
            ChooseInSelect(myChoiceCustomFilter, result);
            BtnLogin.Click();
        }
        public bool OnLoginScreen()
        {
            bool condition = false; ;
            //Console.WriteLine("Este é o valor da condição no incio "+condition);
            condition = ImgOverTitle.Displayed;
            condition = TitleLogin.Text == ("Plataforma Hypervision");
            condition = InputLogin.Displayed;
            condition = InputPassword.Displayed;
            condition = BtnShowPassword.Displayed;
            condition = CustomFilter.Displayed;
            condition = BtnLogin.Displayed;
            //Console.WriteLine("Este é o valor da condição no final  "+condition);
            return condition;
        }
    }
}